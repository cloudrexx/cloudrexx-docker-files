# This file is used as a template for ./cx init
# The "image" option always needs to be right after the service name. Otherwise
# ./cx will not be able to re-extract values from the parsed file.
#
# IMPORTANT: Do not change the name of the env manually as it would otherwise
#            reset the db container!
# CLX_ENV_NAME=<env-name>
#
services:
  web:
    image: "<php-image>"
    hostname: "<hostname>"<novhost>
    ports:
      - "<port>:80"</novhost>
    volumes:
      - <cd>:/var/www/html
    environment:<vhost>
      - VIRTUAL_HOST=<virtual-hosts></vhost>
      - HTTPS_METHOD=noredirect
      - CLX_DEV_MODE=<dev-mode>
      - CLX_HTTPD_EXTRA_CONF=<httpd-extra-conf>
      - CLX_FPM_LISTEN_UDS=0
      - CLX_FPM_CONTAINER_HOST=php
      - CLX_ADMIN_EMAIL=<email>
      - CLX_MEMORY_LIMIT=<httpd-memory-limit>
      - CLX_HARD_LIMIT_WORKERS=<httpd-worker-limit>
    restart: always<mod_php>
    depends_on:
      - db
      - usercache</mod_php>
    networks:
      - front-end
      - back-end<php-fpm>
  php:
    image: "<php-fpm-image>"
    volumes:
      - <cd>:/var/www/html
    environment:
      - CLX_DEV_MODE=<dev-mode>
      - CLX_FPM_LISTEN_UDS=0
      - CLX_FPM_LISTEN_CLIENT_HOST=web
      - CLX_DBG_FLAGS='<dbg-flags>'
    restart: always
    depends_on:
      - web
      - db
      - usercache
    networks:
      - back-end<vhost>
      - <envs-dns-name>
    dns:
      - <envs-dns-ip>
    extra_hosts:
      - "<hostname>:<envs-proxy-ip>"</vhost>
  cron:
    image: "<php-fpm-image>"
    volumes:
      - <cd>:/var/www/html
    command: ["cron", "-f", "-L", "2"]
    init: true
    environment:
      - CLX_DEV_MODE=<dev-mode>
      - CLX_DBG_FLAGS='<dbg-flags>'
    restart: always
    depends_on:
      - php
    networks:
      - back-end<vhost>
      - <envs-dns-name>
    dns:
      - <envs-dns-ip></vhost></php-fpm>
  db:
    image: "<db-image>"
    command: --sql-mode="NO_ENGINE_SUBSTITUTION,NO_AUTO_VALUE_ON_ZERO" --character-set-server=utf8 --collation-server=utf8_unicode_ci
    volumes:<persistdb>
      - db-data:/var/lib/mysql</persistdb>
      - <cd>/tmp/db:/tmp/import
    networks:
      - back-end<db-init-config>
    environment:
      - MARIADB_ROOT_PASSWORD=<db-pass>
      - MARIADB_DATABASE=<db-name></db-init-config>
    restart: always<with-mail>
  mail:
    image: mailhog/mailhog
    hostname: "mail.<hostname>"<novhost>
    ports:
      - "8025:8025"</novhost>
    user: root
    environment:
      - MH_SMTP_BIND_ADDR=0.0.0.0:25<vhost>
      - MH_API_BIND_ADDR=0.0.0.0:80
      - MH_UI_BIND_ADDR=0.0.0.0:80
      - MH_HOSTNAME=mail.<hostname>
      - VIRTUAL_HOST=mail.<hostname>
    restart: always
    expose:
      - 80</vhost>
    networks:
      - front-end
      - back-end</with-mail><with-phpmyadmin>
  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    hostname: "phpma.<hostname>"
    environment:
      - PMA_ARBITRARY=1
      - PMA_HOST=db
      - PMA_USER=<pma-user>
      - PMA_PASSWORD=<pma-password><vhost>
      - VIRTUAL_HOST=phpma.<hostname></vhost>
    networks:
      - front-end
      - back-end
    depends_on:
      - db
    restart: always<novhost>
    ports:
      - 8234:80</novhost></with-phpmyadmin>
  usercache:
    image: "cloudrexx/memcached:1.6"
    restart: always
    networks:
      - back-end
networks:
  back-end:
  front-end:<vhost>
    external: true
    name: <proxy-network>
  <envs-dns-name>:
    external: true</vhost>
volumes:<persistdb>
  db-data:</persistdb>
