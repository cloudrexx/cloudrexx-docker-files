#!/bin/sh
set -e

# TOOD: Add memcache user to supplied group, drop memcache group

# if env var is set, use UDS
if [ "$MEMCACHED_UDS" = true ]; then
    echo "Using UNIX Domain Socket"
    # ensure folder with correct permissions exists
    if [ ! -d "/var/run/docker/memcached" ]; then
        if ! mkdir -p "/var/run/docker/memcached"; then
            echo "Could not create directory"
            exit
        fi
        if ! chmod -R "$MEMCACHED_UDS_PERMISSION" "/var/run/docker/memcached"; then
            echo "Could not chmod directory"
            exit
        fi
    else
        # TODO: check permissions
        :
    fi
    if [ -f "/var/run/docker/memcached/memcached.sock" ]; then
        rm "/var/run/docker/memcached/memcached.sock"
    fi
    set -- "$@" -s /var/run/docker/memcached/memcached.sock -a "0$MEMCACHED_UDS_PERMISSION"
fi

# if env var is set, set ram
if [ -n "${MEMCACHED_MEMORY}" ]; then
    echo "Using ${MEMCACHED_MEMORY}MB of memory"
    set -- "$@" -m "$MEMCACHED_MEMORY"
fi

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- memcached "$@"
fi

exec "$@"
