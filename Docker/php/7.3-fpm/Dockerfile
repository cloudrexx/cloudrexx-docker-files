FROM php:7.3-fpm
RUN bash -c " \
    # Enable non-interactive usage of apt (required by msmtp-mta)
           export DEBIAN_FRONTEND=noninteractive \
    # Debian packages
        && apt update \
        && apt install -y \
            # Add ip command - used by clx-docker-php-entrypoint
            iproute2 \
            # Add jq - used by clx-docker-php-entrypoint
            jq \
            # Add SMTP-MTA - used by \Cx\Core\MailTemplate
            msmtp-mta \
            # Add /etc/services - used by \Cx\Core\Routing\Url
            netbase \
            # Add crond - used for cx Cron
            cron \
            # provides envsubstr - used by clx-docker-php-entrypoint
            gettext-base \
            # Add cgi-fcgi - used to run cx from CLI through PHP-FPM
            libfcgi0ldbl \
    # Drop default crontabs
        && rm -rf /etc/cron.* \
    # Add helper-script to install PHP extensions
        && curl -sSLf \
            -o /usr/local/bin/install-php-extensions \
            https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions \
        && chmod +x /usr/local/bin/install-php-extensions \
    # PHP extensions
        && install-php-extensions \
            # bundled extensions
            bcmath \
            ctype \
            exif \
            fileinfo \
            filter \
            iconv \
            gd \
            intl \
            mbstring \
            opcache \
            pdo \
            posix \
            session \
            sockets \ 
            tokenizer \ 
            zlib \ 
            # external extensions
            curl \ 
            dom \ 
            igbinary \
            libxml \
            mysqlnd \ 
            pdo_mysql \
            simplexml \
            xml \
            xmlwriter \
            zip \
            # pecl extensions
            imagick \
            memcached-3.1.5 \
    # Disable not needed extensions
        && rm $PHP_INI_DIR/conf.d/docker-php-ext-sodium.ini \
    # Cleanup
        && apt-get autoremove -y \
        && apt-get clean"
COPY crontab /etc/crontab
COPY clx-php.ini $PHP_INI_DIR/
COPY clx-php-fpm.conf /usr/local/etc/clx-php-fpm.conf
COPY clx-php-fpm-cli /usr/local/bin/
COPY clx-docker-php-entrypoint /usr/local/bin/
ENV CLX_DEV_MODE="0"
ENV CLX_DOCUMENT_ROOT="/var/www/html"
ENV CLX_FPM_LISTEN_UDS="0"
ENV CLX_FPM_CONFIG_LISTEN_MODE="0660"
ENV CLX_FPM_LISTEN_PORT="9000"
ENV CLX_FPM_LISTEN_CLIENT_HOST="web"
ENV CLX_FPM_LISTEN_CLIENT_IP=
ENV CLX_FPM_LISTEN_DEV="eth0"
ENV CLX_FPM_CONFIG_PM_MAX_CHILDREN=
ENV CLX_FPM_CONFIG_ACCESS_LOG_FORMAT="%R - %u %t \"%m %r\" %s"
ENV CLX_SMTP_MTA_HOST="mail"
ENV CLX_DBG_FLAGS="'DBG_PHP | DBG_LOG_FILE'"
ENV CLX_PHP_CONFIG_AUTO_PREPEND_FILE="lib/FRAMEWORK/DBG/DBG.php"
ENTRYPOINT ["clx-docker-php-entrypoint"]
CMD ["php-fpm"]
