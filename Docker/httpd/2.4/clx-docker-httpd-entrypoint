#!/bin/bash
if [[ "$CLX_DEV_MODE" -lt 0 ]] || [[ "$CLX_DEV_MODE" -gt 1 ]]; then
    export CLX_DEV_MODE=0
fi
if [[ $CLX_DEV_MODE -eq 0 ]]; then
    CLX_MODE="production"
else
    CLX_MODE="development"
fi
echo "Init in $CLX_MODE mode"
echo "-> Change by setting CLX_DEV_MODE=$(( ! $CLX_DEV_MODE ))"

export CLX_HOSTNAME="${CLX_HOSTNAME:-$(hostname --fqdn)}"

export CLX_DOCUMENT_ROOT="${CLX_DOCUMENT_ROOT%%+(/)}"
if [[ -z "$CLX_DOCUMENT_ROOT" ]]; then
    echo "Invalid document root specified." >&2
    exit
fi
if [[ ! -d "$CLX_DOCUMENT_ROOT" ]]; then
    echo "No data mapped to document root $CLX_DOCUMENT_ROOT" >&2
    exit
fi

if [[ "$CLX_FPM_LISTEN_UDS" -eq 1 ]]; then
    CLX_FPM_LISTEN_MODE="uds"
    export CLX_FPM_CONFIG_LISTEN="unix:~HTTPD_PREFIX/run/php-fpm.sock|fcgi://127.0.0.1:9000"
else
    CLX_FPM_LISTEN_MODE="tcp"
    export CLX_FPM_CONFIG_LISTEN="fcgi://$CLX_FPM_CONTAINER_HOST:$CLX_FPM_LISTEN_PORT"
fi
echo "Proxy PHP to PHP-FPM through ${CLX_FPM_LISTEN_MODE^^} ($CLX_FPM_CONFIG_LISTEN)"
echo "-> Change by setting CLX_FPM_LISTEN_UDS=$(( ! $CLX_FPM_LISTEN_UDS ))"

if [[ "$CLX_FPM_LISTEN_UDS" -eq 0 ]]; then
    echo "-> or CLX_FPM_CONTAINER_HOST=<hostname> and CLX_FPM_LISTEN_PORT=<port>"
fi

function get_memory_limit {
    if [[ -n "${CLX_MEMORY_LIMIT}" ]]; then
        CLX_MEMORY_LIMIT="$(numfmt --from=iec ${CLX_MEMORY_LIMIT^^})"
    fi
    DEFAULT_MEMORY_LIMIT="$(numfmt --from=iec 100M)"
    CLX_MEMORY_LIMIT="${CLX_MEMORY_LIMIT:-$DEFAULT_MEMORY_LIMIT}"
    CONTAINER_MEMORY_LIMIT=$(cat /sys/fs/cgroup/memory/memory.limit_in_bytes)
    if [[ $CLX_MEMORY_LIMIT -gt $CONTAINER_MEMORY_LIMIT ]]; then
        CLX_MEMORY_LIMIT=$CONTAINER_MEMORY_LIMIT
    fi
    echo $CLX_MEMORY_LIMIT
}
function get_httpd_worker_process_memory_usage {
    local CLX_HTTPD_MEMORY_USAGE_BY_THREAD="$(numfmt --from=iec 80K)"
    local CLX_HTTPD_MEMORY_USAGE_BY_PROCESS=$(( $CLX_HTTPD_MEMORY_USAGE_BY_THREAD * $CLX_MPM_THREADSPERCHILD ))
    local SHARED_MEMORY_USAGE="$(numfmt --from=iec 22K)"
    echo $(( $CLX_HTTPD_MEMORY_USAGE_BY_PROCESS + $SHARED_MEMORY_USAGE ))
}
function get_httpd_main_process_memory_usage {
    numfmt --from=iec 3.8M
}
function set_httpd_mpm_production_mode {
    CLX_MEMORY_LIMIT=$(get_memory_limit)

    # ThreadsPerChild
    #   Description: Number of threads created by each child process
    #   Default: 25
    export CLX_MPM_THREADSPERCHILD=25

    # ServerLimit
    #   Description: Upper limit on configurable number of processes
    #   Default: 16
    CLX_MPM_SERVERLIMIT=$(( ( $CLX_MEMORY_LIMIT - $(get_httpd_main_process_memory_usage) ) / ( $(get_httpd_worker_process_memory_usage) ) ))
    CLX_HARD_LIMIT_SERVER=$(( $CLX_HARD_LIMIT_WORKERS / $CLX_MPM_THREADSPERCHILD ))
    export CLX_MPM_SERVERLIMIT=${CLX_MPM_SERVERLIMIT%%.*}
    if [[ $CLX_MPM_SERVERLIMIT -eq 0 ]]; then
        export CLX_MPM_SERVERLIMIT=1
        export CLX_MPM_THREADSPERCHILD=1
    elif [[ $CLX_MPM_SERVERLIMIT -gt $CLX_HARD_LIMIT_SERVER ]]; then
        export CLX_MPM_SERVERLIMIT=$CLX_HARD_LIMIT_SERVER
    fi

    # StartServers: initial number of server processes to start
    #   Description: Number of threads created on startup
    #   Default: 3
    export CLX_MPM_STARTSERVERS=$CLX_MPM_SERVERLIMIT

    # MaxRequestWorkers
    #   Description: Maximum number of connections that will be processed simultaneously
    #   Default: 400
    export CLX_MPM_MAXREQUESTWORKERS=$(( $CLX_MPM_SERVERLIMIT * $CLX_MPM_THREADSPERCHILD ))
    echo "MaxRequestWorkers set to $CLX_MPM_MAXREQUESTWORKERS"
    echo "-> Adjust by increasing/decreasing CLX_MEMORY_LIMIT=<limit> (currently set to: $(numfmt --to=iec ${CLX_MEMORY_LIMIT})) or CLX_HARD_LIMIT_WORKERS=<limit> (currently set to: ${CLX_HARD_LIMIT_WORKERS})"
    echo "   WARNING: Setting to high values here will most likely crash the host system!"

    # MaxSpareThreads
    #   Description: Maximum number of idle threads
    #   Default: 250
    export CLX_MPM_MAXSPARETHREADS=$CLX_MPM_MAXREQUESTWORKERS

    # MinSpareThreads
    #   Description: Minimum number of idle threads available to handle request spikes
    #   Default: 75
    export CLX_MPM_MINSPARETHREADS=$CLX_MPM_MAXSPARETHREADS

    # MaxConnectionsPerChild
    #   Description: Limit on the number of connections that an individual child server will handle during its life
    #   Default: 0
    export CLX_MPM_MAXCONNECTIONSPERCHILD=0

    # ThreadLimit
    #   Description: Sets the upper limit on the configurable number of threads per child process
    #   Default: 64
    export CLX_MPM_THREADLIMIT=64
    if [[ $CLX_MPM_THREADLIMIT -lt $CLX_MPM_THREADSPERCHILD ]]; then
        export CLX_MPM_THREADLIMIT=$CLX_MPM_THREADSPERCHILD
    fi
}
function set_httpd_mpm_development_mode {
    export CLX_MPM_STARTSERVERS=1
    export CLX_MPM_MINSPARETHREADS=1
    export CLX_MPM_MAXSPARETHREADS=1
    export CLX_MPM_THREADSPERCHILD=25
    export CLX_MPM_MAXREQUESTWORKERS=25
    export CLX_MPM_MAXCONNECTIONSPERCHILD=0
    export CLX_MPM_SERVERLIMIT=1
    export CLX_MPM_THREADLIMIT=25
}

if [[ "${CLX_MPM_AUTO_CONFIG}" -ne 0 ]]; then
    if [[ $CLX_DEV_MODE -eq 0 ]]; then
        set_httpd_mpm_production_mode
    else
        set_httpd_mpm_development_mode
    fi
fi

httpd_conf_dir="$HTTPD_PREFIX/conf"
httpd_conf="$httpd_conf_dir/httpd.conf"

# setup modules
clx_modules_conf="$httpd_conf_dir/clx/modules.conf"
if [[ ! -f "$clx_modules_conf" ]]; then
    echo "Invalid modules.conf source: $clx_modules_conf --> abort"
    exit 1
fi
# disable all modules
sed -i -e 's/^\s*\(LoadModule\)/#\1/g' "$httpd_conf"
modules="$(cat "$clx_modules_conf")"
# add mod_info and mod_status in dev mode
if [[ $CLX_DEV_MODE -eq 1 ]]; then
    modules="$(echo -e "$modules\ninfo\nstatus\nauthz_host")"
fi
# enable selected modules
IFS=$'\n'
for module in $modules; do
    if [[ "$module" =~ ^[[:space:]]*# ]]; then
        continue;
    fi
    sed -i -e "s/^\s*\#\(LoadModule\s\+${module}_module\s\+.*$\)/\1/" "$httpd_conf"
done
unset IFS

# drop default sections
function drop_directory {
    local httpd_conf="$1"
    local regex="(()<Directory \"${2//\//\\/}\">[^<]*</Directory>())"
    local HAYSTACK="$(cat "$httpd_conf")"
    local OLDHAYSTACK=""

    while [[ "$HAYSTACK" != "$OLDHAYSTACK" ]]; do
        OLDHAYSTACK="$HAYSTACK"
        HAYSTACK="$(regex_replace "$HAYSTACK" "$regex" "")"
    done
    echo "$HAYSTACK" > "$httpd_conf"
}
function regex_replace {
    local HAYSTACK="$1"
    local REGEX="$2"
    local REPLACEMENT="$3"
    [[ "$HAYSTACK" =~ $REGEX ]]
    REPLACE="${BASH_REMATCH[2]}${REPLACEMENT}${BASH_REMATCH[3]}"
    HAYSTACK="${HAYSTACK//"${BASH_REMATCH[0]}"/"$REPLACE"}"
    echo "$HAYSTACK"
}
drop_directory "$httpd_conf" "$HTTPD_PREFIX/htdocs"
drop_directory "$httpd_conf" "$HTTPD_PREFIX/cgi-bin"

# set options
IFS=$'\n'
for option in $(cat "$httpd_conf_dir/clx/httpd.conf"); do
    if [[ "$option" =~ ^[[:space:]]*# ]]; then
        continue;
    fi
    name="${option%% *}"
    value="${option#* }"
    regex="^\#?$name\s.*\$"
    grep -q -m1 -P "$regex" "$httpd_conf"
    if [[ $? -eq 0 ]]; then
        sed -i -E -e "s/$regex/$name ${value//\//\\/}/" "$httpd_conf"
    else
        echo "$name $value" >> "$httpd_conf"
    fi
done
unset IFS

# enable /server-status & /server-info in dev mode
if [[ $CLX_DEV_MODE -eq 1 ]]; then
    echo "Include conf/clx/info.conf" >> "$httpd_conf"
fi

# include extra httpd conf
if [[ -n "$CLX_HTTPD_EXTRA_CONF" ]] && [[ -f "$CLX_DOCUMENT_ROOT/$CLX_HTTPD_EXTRA_CONF" ]]; then
    echo "Include $CLX_DOCUMENT_ROOT/$CLX_HTTPD_EXTRA_CONF" >> "$httpd_conf"
fi

echo "Include conf/clx/clx.conf" >> "$httpd_conf"

usermod -u $(stat -c '%u' "$CLX_DOCUMENT_ROOT") www-data
groupmod -g $(stat -c '%g' "$CLX_DOCUMENT_ROOT") www-data

if [[ $CLX_DEV_MODE -eq 1 ]]; then
    apachectl -l >&2
    apachectl -S >&2
    apachectl -M >&2
fi

exec "$@"
