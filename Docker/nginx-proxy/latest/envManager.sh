#!/bin/bash

echo "<h1>Existing Hosts</h1>"
servers=$(grep "server_name " /etc/nginx/conf.d/default.conf | grep -v "server_name _")
if [[ "$servers" == "" ]]; then
    echo "No hosts up..."
    exit
fi
echo "<ul>"
while read -r line; do
    server="${line//"server_name "/}"
    server="${server//";"/}"
    if [[ "$server" =~ ^(mail|phpma)\. ]]; then
        continue
    fi
    env="${server//".clx.dev"/}"
    # legacy root-domain
    env="${env//".lvh.me"/}"
    echo "<li>$env<ul>"
    echo "<li><a href=\"http://$server\">$server</a></li>"
    phpmaserver="phpma.$server"
    if [[ "$servers" =~ \ ${phpmaserver//\./\\.} ]]; then
        echo "<li><a href=\"http://$phpmaserver\">phpMyAdmin</a></li>"
    fi
    mailserver="mail.$server"
    if [[ "$servers" =~ \ ${mailserver//\./\\.} ]]; then
        echo "<li><a href=\"http://$mailserver\">MailHog</a></li>"
    fi
    echo "</ul></li>"
done <<< "$servers"
echo "</ul>"
