# Cloudrexx Docker images #
This repository contains the Dockerfiles for all Docker images used by `cx env`
(see [Dockerhub](https://hub.docker.com/r/cloudrexx/)).

You will find the Dockerfiles for each image in the `/Docker` folder.
Current images are:

- [PHP images](#markdown-header-php-images)
- [Envs Proxy](#markdown-header-envs-proxy)
- [Envs Proxy DNS](#markdown-header-envs-proxy-dns)
- [Apache](#markdown-header-apache)
- [Memcached](#markdown-header-memcached)
- [Ubuntu](#markdown-header-ubuntu)
- [rbldnsd](#markdown-header-rbldnsd)

## Proposed setup ##
Please check the readme on https://bitbucket.org/cloudrexx/cloudrexx-oss/ for
instructions on how to setup a Cloudrexx environment based on these images.

## (Re-)Build images ##
In order to (re-)build the images, do simply run:
```bash
./build.sh
```

## PHP images ##
All images are based on the [official PHP images](https://hub.docker.com/_/php/).

For each PHP version supported by Contrexx/Cloudrexx (v3.0 upwards, starting at
PHP 5.6) there are several image variants (i.e. `php:8.0-fpm`):

- With suffix `-fpm` (starting with PHP `7.2`)
- *without suffix* (only until PHP `7.1`)
- With suffix `-mysql` (only until PHP `7.1`)

### PHP-FPM ###
The images suffixed with `-fpm` are based on the official Debian PHP-FPM image
variant (`php:<version>-fpm`) and do just contain PHP-FPM.

#### Operation mode ####
The PHP-FPM service container runs in production mode by default. Optionally it
can be run in development mode (by setting environment variable `CLX_DEV_MODE`
to `1`) which is the recommended mode for local development purposes as it
provides more debug insights and uses less resources.

|   | **Development** | **Production** |
|---|---|---|
| **Process Manager mode** (`pm`) | `dynamic` | `static` |
| **Number of child processes** (`pm.max_children`) | `5` | `100` |
| **Generate assertion code in PHP** (`zend.assertions`) | Yes | No |
| **Show startup errors** (`display_startup_errors`) | `1` | `0` |

#### Environment configuration ####
| **Variable** | **Default** | **Description** |
|---|---|---|
| `CLX_DEV_MODE` | `0` (*Production*) | Set to `1` to enable development mode. |
| `CLX_DBG_FLAGS` | If `CLX_DEV_MODE` is set to `0` (*Production*), then `'DBG_PHP | DBG_LOG_FILE'`, otherwise `'DBG_PHP | DBG_LOG_FILE | DBG_DEBUG | DBG_DB_ERROR'` | Configure DBG debugging mode. https://wiki.cloudrexx.com/Development_Debugging#Env |
| `CLX_DOCUMENT_ROOT` | `/var/www/html` | Set path to where the *Document-Root* has been mounted to.<br>**IMPORTANT**: The *Document-Root* path has to be equal to the one of the web service that is using this *PHP-FPM* service. When using the *Apache* container of Cloudrexx, this must be set to the same value as `CLX_DOCUMENT_ROOT` of the that container. |
| `CLX_FPM_LISTEN_UDS` | `0` (*TCP*) | Set to `1` to let the *PHP-FPM* service listen on a *Unix domain socket*. Default is to listen over `TCP`. |
| `CLX_FPM_CONFIG_LISTEN_MODE`\* | `0600` | Set permission mode (`listen.mode`) for *Unix domain socket*. |
| `CLX_FPM_LISTEN_PORT`\*\* | `9000` | Set TCP port on which the *PHP-FPM* service shall listen on. |
| `CLX_FPM_LISTEN_CLIENT_HOST`\*\*\* | `web` | Set hostname of the web service that will use this *PHP-FPM* service.<br>**Note:** The use of only one upstream server is currently supported. |
| `CLX_FPM_LISTEN_CLIENT_IP` | Will be set to the resolved ip of `CLX_FPM_LISTEN_CLIENT_HOST` (if set). | Set TCP IP address of the httpd service that will use the *PHP-FPM* container (to set `listen.allowed_clients`).<br>**Note:** The use of only one upstream server is currently supported. |
| `CLX_FPM_LISTEN_DEV`\*\* | `eth0` | Set network device that will be used by the *PHP-FPM*  service to listen to. |
| `CLX_FPM_CONFIG_PM_MAX_CHILDREN` | If `CLX_DEV_MODE` is set to `0` (*Production*), then `100`, otherwise `5` | Set the [pm.max_children](https://www.php.net/manual/en/install.fpm.configuration.php#pm.max-children) of the *PHP-FPM* service. |
| `CLX_FPM_CONFIG_ACCESS_LOG_FORMAT` | `"%R - %u %t \"%m %r\" %s"` | Set the [access log format](https://www.php.net/manual/en/install.fpm.configuration.php#access-format) of the *PHP-FPM* service. |
| `CLX_SMTP_MTA_HOST` | `mail` | Set hostname of SMTP MTA to use for mail relay.<br>**Note:** The MTA must accept mail submission over port 25. |
| `CLX_PHP_CONFIG_AUTO_PREPEND_FILE` | `lib/FRAMEWORK/DBG/DBG.php` | Set the [auto_prepend_file](https://www.php.net/manual/en/ini.core.php#ini.auto-prepend-file) directive.<br>**Note:** Set to an empty string when not running a Cloudrexx env. |
| `CLX_CONFIG_<OPTION>`\*\*\*\* |  | Set Cloudrexx option `$_CONFIG[<OPTION>]`. |
| `CLX_DBCONFIG_<OPTION>`\*\*\*\* |  | Set Cloudrexx option `$_DBCONFIG[<OPTION>]`. |
| `CLX_IP_MGMT_TZ` | `UTC` | Set timezone used by IP-management service. |

\* Only applicable if `CLX_FPM_LISTEN_UDS` is set to `1`

\*\* Only applicable if `CLX_FPM_LISTEN_UDS` is set to `0`

\*\*\* Only applicable if `CLX_FPM_LISTEN_CLIENT_IP` is not set

\*\*\*\* `<OPTION>` corresponds to a valid index from the associated PHP array (`$_CONFIG`, `$_DBCONFIG`). Where the origin option index (which is in camelCase notation) is exploded by each uppercase character, followed by an underscore and converted into capitals to form the notation of `<OPTION>`. I.e. the associated environment variable for option `$_CONFIG['cacheUserCacheMemcachedConfig']` is `CLX_CONFIG_CACHE_USER_CACHE_MEMCACHED_CONFIG`. Or for `$_DBCONFIG['host']` it is `CLX_DBCONFIG_HOST`.

### mod_php ###
The images without any suffix (i.e. `php:7.0`) are based on the offical
`php:<version>-apache`) image variants and do contain Debian's Apache httpd in
conjunction with PHP as `mod_php`.
Note: Those images are deprecated in favor of the `-fpm` images.

#### Known issues ####
* The PHP 5.6 images do not support Memcached.
* Socket timeout is set to 600s which is way too high.
* PHP is rebuilt 3 times during image build. This could be reduced to once.

### With MySQL ###
The images suffixed with `-with-mysql` are based on the *mod_php* images. In
addition they contain the Debian package `mysql-client` (as of PHP 7.1 the
package `mariadb-client` is used). This can be useful for maintenance and
debugging on systems without phpMyAdmin or any other access to the database
server.
Note: Those images are deprecated in favor of the `-fpm` images and are no
longer available starting with PHP 7.2.

## Other images ##
### Envs Proxy ###
The folder `/Docker/nginx-proxy` contains the image definition for the Nginx reverse
proxy used by `cx envs`.

### Envs Proxy DNS ###
The folder `/Docker/dnsmasq` contains the image definition for the DNS service
used by `cx envs` and each `cx env` to ensure all inter-container communication
is routed through the `cx envs`-proxy.

### Apache ###
The folder `/Docker/httpd` contains the image definition for the Apache
httpd service used in conjunction with the *PHP-FPM* images.

#### Operation mode ####
The Apache service container runs in production mode by default. Optionally it
can be run in development mode (by setting environment variable `CLX_DEV_MODE`
to `1`) which is the recommended mode for local development purposes as it
provides more debug insights and uses less resources.

|   | **Development** | **Production** |
|---|---|---|
| **Server status location** | Yes: `/server-status` | No |
| **Server info location** | Yes: `/server-info` | No |
| **Dumping configuration on startup** | Yes | No |
| **Number of worker processes** \* | `1` | Maximum possible based on `CLX_MEMORY_LIMIT` and `CLX_HARD_LIMIT_WORKERS` |

\* Only applicable if `CLX_MPM_AUTO_CONFIG` is set to `1`

#### Environment configuration ####
| **Variable** | **Default** | **Description** |
|---|---|---|
| `CLX_HOSTNAME` | Hostname of container set through `--hostname` option of `docker run` | Set hostname to be set as `ServerName` of Apache. |
| `CLX_ADMIN_EMAIL` | `info@example.org` | Set email address to be set as `ServerAdmin` of Apache. |
| `CLX_DEV_MODE` | `0` (*Production*) | Set to `1` to enable development mode. |
| `CLX_FPM_LISTEN_UDS` | `0` (*TCP*) | Set to `1` to enable *Unix domain socket* communication with *PHP-FPM* container. Default is to use `TCP`. |
| `CLX_FPM_LISTEN_PORT` | `9000` | Set port of *PHP-FPM* container to connect to when using `TCP` connection (`CLX_FPM_LISTEN_UDS`=`0`). |
| `CLX_FPM_CONTAINER_HOST` | `php` | Set hostname of *PHP-FPM* container to connect to when using `TCP` connection (`CLX_FPM_LISTEN_UDS`=`0`). |
| `CLX_DOCUMENT_ROOT` | `/var/www/html` | Set path to where the *Document-Root* has been mounted to.<br>**IMPORTANT**: This must be set to the same value as `CLX_DOCUMENT_ROOT` of the PHP-FPM container. |
| `CLX_HTTPD_EXTRA_CONF` |  | Set to an extra conf file (relative to `CLX_DOCUMENT_ROOT`) to be included into the httpd configuration at start-up.<br>**IMPORTANT**: The conf file must be mounted into the container. |
| `CLX_HTTPD_TIMEOUT` | `120` | Set `TimeOut` directive of Apache. |
| `CLX_MPM_AUTO_CONFIG` | `1` | Set to `0` for manual Apache event MPM configuration. If set to `1` MPM configuration is done automatically based on the values of `CLX_MEMORY_LIMIT` and `CLX_HARD_LIMIT_WORKERS`. |
| `CLX_MEMORY_LIMIT`\* | `100M` or less if restricted by option `--memory` of `docker run` | Set available memory to be used for spawning Apache worker processes.<br>**WARNING**: Setting this higher than the physical available memory will most likely crash the host system! |
| `CLX_HARD_LIMIT_WORKERS`\* | `1024` | Set maximum limit of Apache worker processes to be spawned.<br>**WARNING**: Setting this higher than the actual available process limit (set by `ulimit -u`) of the host system will most likely crash the host system! |
| `CLX_MPM_STARTSERVERS`\*\* |  | Directly set Apache directive `StartServers`. |
| `CLX_MPM_MINSPARETHREADS`\*\* |  | Directly set Apache directive `MinSpareThreads`. |
| `CLX_MPM_MAXSPARETHREADS`\*\* |  | Directly set Apache directive `MaxSpareThreads`. |
| `CLX_MPM_THREADSPERCHILD`\*\* |  | Directly set Apache directive `ThreadsPerChild`. |
| `CLX_MPM_MAXREQUESTWORKERS`\*\* |  | Directly set Apache directive `MaxRequestWorkers`. |
| `CLX_MPM_MAXCONNECTIONSPERCHILD`\*\* |  | Directly set Apache directive `MaxConnectionsPerChild`. |
| `CLX_MPM_SERVERLIMIT`\*\* |  | Directly set Apache directive `ServerLimit`. |
| `CLX_MPM_THREADLIMIT`\*\* |  | Directly set Apache directive `ThreadLimit`. |

\* Only applicable if `CLX_MPM_AUTO_CONFIG` is set to `1`

\*\* Only applicable if `CLX_MPM_AUTO_CONFIG` is set to `0`

### Memcached ###
The folder `/Docker/memcached` contains the image definition for the memcached
usercache service used by *cx env*.

#### Environment configuration ####
| **Variable** | **Default** | **Description** |
|---|---|---|
| `MEMCACHED_MEMORY` | `64`  | Set available memory (in MB) to *memcached* service. |
| `MEMCACHED_UDS` | `false` (*TCP*) | Set to `true` to let the *memcached* service listen on a *Unix domain socket*. |
| `MEMCACHED_UDS_PERMISSION` | `770` | Set permission mode for the *Unix domain socket*. |

### Ubuntu ###
The folder `/Docker/ubuntu` contains the image used by *cx env* as a wrapper
container for non-Bash 4 systems (like Windows or MacOS).

### rbldnsd ###
The folder `/Docker/rbldnsd` contains the image used for running a
[rbldnsd](https://rbldnsd.io/) container.
