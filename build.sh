#!/bin/bash
if [[ -z "$1" ]]; then
    IMAGE_PATH="$(pwd)/"
else    
    IMAGE_PATH="$(realpath "${1%%/}")/"
fi
if [[ -f "${IMAGE_PATH}Dockerfile" ]]; then
    list="${IMAGE_PATH}Dockerfile"
else
    list="$(find "$IMAGE_PATH" -type f -name Dockerfile | sort 2>/dev/null)"
    if [[ -z "$list" ]]; then
        echo "No Dockerfile found in $IMAGE_PATH"
        exit 1
    fi
fi

function get_tag {
    local IMAGE_PATH="$1"
    local DOCKER_HUB_REPO="cloudrexx"
    local NAME
    NAME="$(basename "$(dirname "$IMAGE_PATH")")"
    local PARENT_NAME
    PARENT_NAME="$(basename "$(dirname "$(dirname "$IMAGE_PATH")")")"
    TAG="$DOCKER_HUB_REPO/${PARENT_NAME}:${NAME}"
    echo "$TAG"
}

if [[ "$(wc -l <<< "$list")" -eq 1 ]]; then
    read -p "Build tag $(get_tag "$list") [Yn]:" -r answer
else
    echo "Available tags:"
    NR=1
    for line in $list; do
        printf '%8s' "[$NR] "
        get_tag "$line"
        NR=$(( NR + 1 ))
    done
    echo ""
    printf '%8s' "[all] "
    echo "Build all listed tags"
    printf '%8s' "[n] "
    echo "Abort"
    read -p "Select tags to build (separate multiple by comma) [all]:" -r answer
fi

BUILD_LIST=""
case $answer in
    [Nn])
        exit
        ;;
    [Yy]|[Aa][Ll][Ll]|"")
        BUILD_LIST="$list"
        ;;
    *)
        IFS=,
        for entry in $answer; do
            #if [[ "$entry" =~ ^\s*[0-9]\+\s*$ ]]; then
            if [[ "$entry" =~ ^\s*[0-9]+\s*$ ]] && [[ "$entry" -gt 0 ]] && [[ "$entry" -lt $NR ]]; then
                BUILD_LIST="$BUILD_LIST$(sed -n ${entry}p <<< "$list")"$'\n'
            else
                echo "Unknown selection $entry" >&2
            fi
        done
        unset IFS
        ;;
esac

for BUILD in $BUILD_LIST; do
    if [[ -z "$BUILD" ]]; then
        continue
    fi
    TAG="$(get_tag "$BUILD")"
    echo -e "Going to build $TAG (from $BUILD):\n"
    docker build --pull=true --no-cache -t "$TAG" "$(dirname "$BUILD")"
done
